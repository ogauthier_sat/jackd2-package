# jackd2-package


## Jammy downgrade package

### Build package

```sh
docker buildx build \
  --progress plain \
  --load \
  -t jackd2-package \
  -f jammy-downgrade/Dockerfile \
.
```

### Copy package

```
docker run \
  -v ${PWD}:/export \
  jackd2-package \
    /bin/bash -c 'cp /build/*.deb /export/'
```

## Focal 1.9.20 jack client shared object backport

### Build Jack

```sh
docker buildx build \
  --progress plain \
  --load \
  -t jack2-focal-backport \
  -f focal-backport/Dockerfile \
.
```

### Copy libjack.so file

```
docker run \
  -v ${PWD}:/export \
  jack2-focal-backport \
    /bin/bash -c 'cp /build/jack2-1.9.20/build/common/libjack.so /export/libjack.so.focal.1.9.20'
```

### Bind mount libjack

Copy shared object to host.

```
scp libjack.so.focal.1.9.20 scenicbox:/home/scenicbox/
```

Add bind mount volume into application container.

```
nano docker-compose.yml
```

```
    volumes:
      - '/home/scenicbox/libjack.so.focal.1.9.20:/usr/lib/x86_64-linux-gnu/libjack.so.0.1.0'
```

Redeploy application.

```
docker compose --profile nvidia down
SCENIC_TAG=4.1.4 SCENIC_CORE_TAG=4.1.4 docker compose --profile nvidia up -d
```