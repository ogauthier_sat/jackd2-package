Format: 3.0 (quilt)
Source: jackd2
Binary: jackd2, libjack-jackd2-0, jackd2-firewire, libjack-jackd2-dev
Architecture: any
Version: 1.9.18~scenic-1
Maintainer: Scenic team <scenic-dev@sat.qc.ca>
Uploaders: Adrian Knoth <adi@drcomp.erfurt.thur.de>, Reinhard Tartler <siretart@tauware.de>
Homepage: https://jackaudio.org/
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/multimedia-team/jackd2
Vcs-Git: https://salsa.debian.org/multimedia-team/jackd2.git
Build-Depends: debhelper-compat (= 13), dh-exec, dh-sequence-python3, libasound2-dev [linux-any], libdb-dev, libdbus-1-dev, libexpat1-dev, libffado-dev [linux-any], libncu>
Package-List:
 jackd2 deb sound optional arch=any
 jackd2-firewire deb sound optional arch=amd64,i386,powerpc
 libjack-jackd2-0 deb libs optional arch=any
 libjack-jackd2-dev deb libdevel optional arch=any
Checksums-Sha1:
 5c0a8e454e810dbee3ba798a23ea541208b31507 987166 jackd2_1.9.18~scenic.orig.tar.gz
 1e5dd7dc2a25c8937fbf84693e605dd97355825d 33080 jackd2_1.9.18~scenic-1.debian.tar.xz
Checksums-Sha256:
 5a1d64118034d58529d68a9af42710f9d4843536257c496118065ea3d3e4ae2c 987166 jackd2_1.9.18~scenic.orig.tar.gz
 f3680739d718667fbdb13bb3e6edecbaf98a5ccd8533ae9d978bc2843d21975f 33080 jackd2_1.9.18~scenic-1.debian.tar.xz
Files:
 967358df648eac4c66adee4746efc6cf 987166 jackd2_1.9.18~scenic.orig.tar.gz
 0d5f323d5f9a75d54edbbc3faea80607 33080 jackd2_1.9.18~scenic-1.debian.tar.xz